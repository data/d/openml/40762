# OpenML dataset: Amazon-test

https://www.openml.org/d/40762

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source:

Dataset creator and donator: ZhiLiu, e-mail: liuzhi8673 '@' gmail.com, institution: National Engineering Research Center for E-Learning, Hubei Wuhan, China


Data Set Information:

dataset are derived from the customers&acirc;&euro;&trade; reviews in Amazon Commerce Website for authorship identification. Most previous studies conducted the identification experiments for two to ten authors. But in the online context, reviews to be identified usually have more potential authors, and normally classification algorithms are not adapted to large number of target classes. To examine the robustness of clasification algorithms, we identified 50 of the most active users (represented by a unique ID and username) who frequently posted reviews in these newsgroups. The number of reviews we collected for each author is 30.


Attribute Information:

attribution includes authors' lingustic style such as usage of digit, punctuation, words and sentences' length and usage frequency of words and so on

#autoxgboost #autoweka

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40762) of an [OpenML dataset](https://www.openml.org/d/40762). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40762/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40762/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40762/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

